//  Copyright 2007-2016 Mir Calculate. http://www.calculate-linux.org
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#include <stdio.h>

// для пароля
#include <sys/types.h>

// для strcpy strlen
#include <string.h>

// для работы с ключами
#include <keyutils.h>
// malloc
#include <stdlib.h>

#define BUF_SIZE 255

char* getKey(char *login)
{
    char *buffer = (char*) malloc(BUF_SIZE+1);
    memset(buffer,0,BUF_SIZE);
    int ret;
    // ищем номер пользовательского ключа
    ret = request_key("user", login, NULL, 0);
    if (ret < 0)
    {
        return buffer;
    };

    // Возвращаем значение ключа
    ret = keyctl_read(ret, buffer, BUF_SIZE);
    if (ret < 0)
    {
        return buffer;
    };
    return buffer;
};

int clearKey(char *login)
{
    int ret;
    key_serial_t dest;
    dest = KEY_SPEC_USER_SESSION_KEYRING;
    // ищем номер пользовательского ключа
    ret = request_key("user", login, NULL, 0);
    if (ret < 0)
    {
        return 1;
    };

    // удаляем ключ
    ret = keyctl_unlink(ret, dest);
    if (ret < 0)
    {
        return 1;
    };
    return 0;
};
