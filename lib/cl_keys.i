%module cl_keys
%newobject getKey;
%inline %{
/* Put header files here or function declarations like below */
extern char* getKey(char*);
extern int clearKey(char*);
%}
%include cl_keys.c
