# -*- coding: utf-8 -*-

# Copyright 2008-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from calculate.lib.datavars import ActionVariable

from calculate.lib.cl_lang import setLocalTranslate

setLocalTranslate('cl_desktop3', sys.modules[__name__])


class VariableAcDesktopMerge(ActionVariable):
    """
    Action variable which has value "on" for install calculate-desktop
    and "down" for uninstall
    """
    nonchroot = True

    def action(self, cl_action):
        if cl_action == "merge":
            return "on"
        return "off"


class VariableAcDesktopProfile(ActionVariable):
    """
    Action variable which has value "on" on user profile setup
    """
    nonchroot = True

    def action(self, cl_action):
        if cl_action in ("desktop",):
            return "on"
        return "off"
