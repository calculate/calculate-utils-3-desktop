# -*- coding: utf-8 -*-

# Copyright 2012-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import sys

from calculate.lib.datavars import VariableError, DataVarsError
from calculate.core.server.func import WsdlBase
from .desktop import DesktopError
from .utils.cl_desktop import ClDesktopLogoutAction, ClDesktopAction
from . import desktop

from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate

_ = lambda x: x
setLocalTranslate('cl_desktop3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class Wsdl(WsdlBase):
    methods = [
        #
        # вывести пользователя из сеанса
        #
        {
            # идентификатор метода
            'method_name': "desktop_logout",
            # категория метода
            'category': __('Desktop'),
            # заголовок метода
            'title': __("User Logout"),
            # иконка для графической консоли
            'image': 'calculate-desktop-logout,system-log-out',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-desktop-logout',
            # права для запуска метода
            'rights': ['userconfigure'],
            # объект содержащий модули для действия
            'logic': {'Desktop': desktop.Desktop},
            # описание действия
            'action': ClDesktopLogoutAction,
            # объект переменных
            'datavars': "desktop",
            'native_error': (VariableError, DataVarsError,
                             DesktopError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': 'logout'},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("User logout"),
                                    normal=('cl_desktop_login',),
                                    next_label=_("Execute"))]},
        #
        # настроить пользовательский сеанс
        #
        {
            # идентификатор метода
            'method_name': "desktop",
            # категория метода
            'category': __('Desktop'),
            # заголовок метода
            'title': __("User Account Configuration"),
            # иконка для графической консоли
            'image': 'calculate-desktop,user-desktop,preferences-desktop',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-desktop',
            # права для запуска метода
            'rights': ['userconfigure'],
            # объект содержащий модули для действия
            'logic': {'Desktop': desktop.Desktop},
            # описание действия
            'action': ClDesktopAction,
            # объект переменных
            'datavars': "desktop",
            'native_error': (VariableError, DataVarsError,
                             DesktopError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': 'desktop',
                        'cl_protect_use_set!': 'off'},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("User account configuration"),
                                    normal=('ur_login',),
                                    expert=('cl_desktop_force_setup_set',
                                            'cl_verbose_set',
                                            'cl_templates_locate'),
                                    next_label=_("Execute"))]},
    ]
